<?php

use App\Models\DiscountCode;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\ShippingMethod;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class OrdersControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testPostOrderCreateForAnonymous(): void
    {
        $response = $this->json('POST', '/api/order', [
            'discount_id' => (string)DiscountCode::factory()->create()->id,
            'shipper_id' => (string)ShippingMethod::factory()->create()->id,
            'payment_method_id' => (string)PaymentMethod::factory()->create()->id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'country' => $this->faker->country,
            'total' => 90,
            'discount' => 10,
            'shipping_cost' => 90,
            'products' => [
                [
                    'id' => (string)Product::factory()->create()->id,
                    'count' => 4
                ]
            ]
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPostOrderCreateForUser(): void
    {
        $user = User::factory()->create(['is_admin' => false]);
        Sanctum::actingAs(
            $user,
            ['*']
        );

        $response = $this->json('POST', '/api/order', [
            'discount_id' => (string)DiscountCode::factory()->create()->id,
            'shipper_id' => (string)ShippingMethod::factory()->create()->id,
            'payment_method_id' => (string)PaymentMethod::factory()->create()->id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'country' => $this->faker->country,
            'total' => 90,
            'discount' => 10,
            'shipping_cost' => 90,
            'products' => [
                [
                    'id' => (string)Product::factory()->create()->id,
                    'count' => 4
                ]
            ]
        ]);

        $response->assertCreated();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('user_id', $user->id)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostOrderCreateForUserAsAdmin(): void
    {
        $admin = User::factory()->create(['is_admin' => True]);
        $user = User::factory()->create(['is_admin' => False]);

        Sanctum::actingAs(
            $admin,
            ['*']
        );

        $response = $this->json('POST', '/api/order', [
            'user_id' => (string)$user->id,
            'discount_id' => (string)DiscountCode::factory()->create()->id,
            'shipper_id' => (string)ShippingMethod::factory()->create()->id,
            'payment_method_id' => (string)PaymentMethod::factory()->create()->id,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'address' => $this->faker->address,
            'zip' => $this->faker->postcode,
            'city' => $this->faker->city,
            'country' => $this->faker->country,
            'total' => 90,
            'discount' => 10,
            'shipping_cost' => 90,
            'products' => [
                [
                    'id' => (string)Product::factory()->create()->id,
                    'count' => 4
                ]
            ]
        ]);

        $response->assertCreated();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('user_id', (string)$user->id)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrderSingleByCodeOk(): void
    {
        $user = User::factory()->create(['is_admin' => False]);

        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for($user)
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        Sanctum::actingAs($user,['*']);

        $response = $this->json('GET', '/api/order/history/' . $order->code);

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('id', $order->id)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrderSingleByCodeUnauthorized(): void
    {
        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for(User::factory()->create(['is_admin' => False]))
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        $user = User::factory()->create(['is_admin' => False]);
        Sanctum::actingAs($user,['*']);

        $response = $this->json('GET', '/api/order/history/' . $order->code);

        $response->assertNotFound();
    }

    /**
     *
     * @return void
     */
    public function testGetOrdersHistoryEmpty(): void
    {
        Order::factory(5)
            ->has(Product::factory()->count(3))
            ->for(User::factory()->create(['is_admin' => False]))
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        $user = User::factory()->create(['is_admin' => False]);
        Sanctum::actingAs($user,['*']);

        $response = $this->json('GET', '/api/order/history/');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(0)
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrdersHistoryNormal(): void
    {
        $user = User::factory()->create(['is_admin' => False]);

        Order::factory(5)
            ->has(Product::factory()->count(3))
            ->for($user)
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();


        Sanctum::actingAs($user,['*']);

        $response = $this->json('GET', '/api/order/history/');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(5)
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrderSingleByHash(): void
    {
        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for(User::factory()->create(['is_admin' => False]))
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        $response = $this->json('GET', '/api/order/a/' . hash('sha256', $order->code));

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('id', $order->id)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrdersListAsAdmin(): void
    {
        $user = User::factory()->create(['is_admin' => True]);

        Sanctum::actingAs($user,['*']);

        $orders = Order::factory(2)
            ->has(Product::factory()->count(3))
            ->for(User::factory()->create(['is_admin' => False]))
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        $response = $this->json('GET', '/api/orders/');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->has(2)
            ->first(fn ($json) =>
            $json->where('id', $orders[0]['id'])
                ->where('code', $orders[0]['code'])
                ->etc()
            )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetOrderSingleAsAdmin(): void
    {
        $user = User::factory()->create(['is_admin' => True]);

        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for($user)
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        Sanctum::actingAs($user,['*']);

        $response = $this->json('GET', '/api/orders/' . $order->id);

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('id', $order->id)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPutOrderEditOk(): void
    {
        $user = User::factory()->create(['is_admin' => True]);

        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for($user)
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        Sanctum::actingAs(
            $user,
            ['*']
        );

        $first_name = $this->faker->firstName;

        $response = $this->json('PUT', '/api/orders/' . $order['id'], [
            'first_name' => $first_name
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('first_name', $first_name)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeleteOrderDeleteAsAdminOk(): void
    {
        $user = User::factory()->create(['is_admin' => True]);

        $order = Order::factory()
            ->has(Product::factory()->count(3))
            ->for($user)
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();

        Sanctum::actingAs(
            $user,
            ['*']
        );

        $response = $this->json('DELETE', '/api/orders/'.$order['id']);

        $response->assertOk();
    }
}
