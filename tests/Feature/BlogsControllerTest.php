<?php

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class BlogsControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetBlogsList(): void
    {
        $blogs = Blog::factory(2)->create();

        $response = $this->json('GET', '/api/blog/articles');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(2)
                 ->first(fn ($json) =>
                    $json->where('id', $blogs[0]['id'])
                        ->where('name', $blogs[0]['name'])
                        ->where('slug', $blogs[0]['slug'])
                        ->etc()
                )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetBlogSingle(): void
    {
        $blog = Blog::factory()->create();

        $response = $this->json('GET', '/api/blog/articles/'.$blog['id'] );

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $blog['name'])
                 ->where('slug', $blog['slug'])
                 ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testGetBlogCategoriesList(): void
    {
        $categories = BlogCategory::factory(2)->create();

        $response = $this->json('GET', '/api/blog/categories');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
        $json->has(2)
            ->first(fn ($json) =>
            $json->where('id', $categories[0]['id'])
                ->where('name', $categories[0]['name'])
                ->where('slug', $categories[0]['slug'])
                ->etc()
            )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetBlogCategorySingle(): void
    {
        $category = BlogCategory::factory()->create();

        $response = $this->json('GET', '/api/blog/categories/'.$category['id'] );

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('name', $category['name'])
            ->where('slug', $category['slug'])
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostBlogCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $image = UploadedFile::fake()->image('blog.webp', 375, 375);

        $response = $this->json('POST', '/api/blog/articles', [
            'image' => $image,
            'image_alt' => $this->faker->word,
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->word,
            'data' => $this->faker->text,
            'lang' => 'fr',
            'country' => null,
            'type' => 'tips',
            'article_type' => null
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPutBlogEditOk(): void
    {
        $blog = Blog::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $name = $this->faker->word;
        $data = $this->faker->word;

        $response = $this->json('PUT', '/api/blog/articles/'.$blog['id'], [
            'name' => $name,
            'data' => $data,
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $name)
                ->where('data', $data)
                ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeleteBlogDeleteOk(): void
    {
        $blog = Blog::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/blog/articles/'.$blog['id']);

        $response->assertOk();
    }



    /**
     *
     * @return void
     */
    public function testPostBlogCategoryCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/blog/categories', [
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->word,
            'desc' => $this->faker->text,
            'active' => true
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPutBlogCategoryEditOk(): void
    {
        $blog = BlogCategory::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $name = $this->faker->word;

        $response = $this->json('PUT', '/api/blog/categories/'.$blog['id'], [
            'name' => $name
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('name', $name)
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeleteBlogCategoryDeleteOk(): void
    {
        $blog = BlogCategory::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/blog/categories/'.$blog['id']);

        $response->assertOk();
    }
}
