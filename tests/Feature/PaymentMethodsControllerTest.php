<?php

use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PaymentMethodsControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetPaymentMethodsList(): void
    {
        $paymentMethods = PaymentMethod::factory(2)->create();

        $response = $this->json('GET', '/api/payments/');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(2)
                 ->first(fn ($json) =>
                    $json->where('id', $paymentMethods[0]['id'])
                        ->where('name', $paymentMethods[0]['name'])
                        ->where('slug', $paymentMethods[0]['slug'])
                        ->etc()
                )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetPaymentMethodSingle(): void
    {
        $paymentMethod = PaymentMethod::factory()->create();

        $response = $this->json('GET', '/api/payments/'.$paymentMethod['slug'] );

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $paymentMethod['name'])
                 ->where('slug', $paymentMethod['slug'])
                 ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostPaymentMethodCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/payments/', [
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->word,
            'cost' => rand(5,99),
            'cost_per_piece' => $this->faker->boolean(90),
            'active' => true
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPutPaymentMethodEditOk(): void
    {
        $paymentMethod = PaymentMethod::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $name = $this->faker->word;
        $slug = $this->faker->word;

        $response = $this->json('PUT', '/api/payments/'.$paymentMethod['slug'], [
            'name' => $name,
            'slug' => $slug,
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $name)
                ->where('slug', $slug)
                ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeletePaymentMethodDeleteOk(): void
    {
        $paymentMethod = PaymentMethod::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/payments/'.$paymentMethod['slug']);

        $response->assertOk();
    }

}
