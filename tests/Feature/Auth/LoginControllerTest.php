<?php

namespace Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Testing Login request
     *
     * @return void
     */
    public function testLoginOk(): void
    {
        $email = 'email@email.com';
        $pwd   = 'pas$$w0rd';

        User::factory()->state([
            'email' => $email,
            'password' => bcrypt($pwd)
        ])->create();

        $response = $this->json('POST', '/api/login', [
            'email' => $email,
            'password' => $pwd,
            'token_name' => 'UnitTest'
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('user.email', $email)
                 ->missing('user.password')
                 ->etc()
        );
    }

    /**
     * Testing Login request with empty form
     *
     * @return void
     */
    public function testLoginPreconditionFailed(): void
    {
        $response = $this->json('POST', '/api/login', [
            'email' => '',
            'password' => '',
            'token_name' => 'UnitTest'
        ]);

        $response->assertStatus(Response::HTTP_PRECONDITION_FAILED);
    }

    /**
     * Testing Login request with invalid password
     *
     * @return void
     */
    public function testLoginUnauthorized(): void
    {
        $email = 'email@email.com';
        $pwd   = 'pas$$w0rd';

        User::factory()->state([
            'email' => $email,
            'password' => bcrypt($pwd)
        ])->create();

        $response = $this->json('POST', '/api/login', [
            'email' => $email,
            'password' => 'FAKEFAKEFAKE',
            'token_name' => 'UnitTest'
        ]);

        $response->assertUnprocessable();
    }

    /**
     * Testing Logout normal
     *
     * @return void
     */
    public function testLogoutOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->json('POST', '/api/logout');

        $response->assertOk();
    }

    /**
     * Testing Logout request without session
     *
     * @return void
     */
    public function testLogoutUnauthorized(): void
    {
        $response = $this->json('POST', '/api/logout');

        $response->assertUnauthorized();
    }
}
