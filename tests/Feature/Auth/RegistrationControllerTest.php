<?php

namespace Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

class RegistrationControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     * User registration attempt
     *
     * @return void
     */
    public function testUserRegistrationOk(): void
    {
        $response = $this->json('POST', '/api/signup', [
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('123456'),
            'token_name' => 'UnitTest'
        ]);

        $response->assertCreated();
    }

    /**
     * User registration attempt with invalid email
     *
     * @return void
     */
    public function testUserRegistrationUnprocessable(): void
    {
        $response = $this->json('POST', '/api/signup', [
            'email' => 'www',
            'password' => bcrypt('123456'),
            'token_name' => 'UnitTest'
        ]);

        $response->assertStatus(406);
    }

    /**
     * Sending process of password's recovery email with existing user
     *
     * @return void
     */
    public function testSendsPasswordResetEmailOk(): void
    {
        // TODO
    }

    /**
     * Allows a user to reset their password.
     *
     * @return void
     */
    public function testChangesAUsersPasswordOk()
    {
        $user =  User::factory()->create();

        $token = Password::createToken($user);

        $response = $this->json('POST', '/api/recovery', [
            'token' => $token,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertOk();

        $this->assertTrue(Hash::check('password', $user->fresh()->password));
    }

    /**
     * Password recovery request with invalid token
     *
     * @return void
     */
    public function testChangesAUsersPasswordUnauthorized(): void
    {
        $user =  User::factory()->create();

        $response = $this->json('POST', '/api/recovery', [
            'token' => 'InvalidToken',
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertUnauthorized();
    }

    /**
     * Password recovery request with undefined user
     *
     * @return void
     */
    public function testChangesAUsersPasswordNotFound(): void
    {
        $user =  User::factory()->create();

        $token = Password::createToken($user);

        $response = $this->json('POST', '/api/recovery', [
            'token' => $token,
            'email' => 'ghost@example.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $response->assertNotFound();
    }
}
