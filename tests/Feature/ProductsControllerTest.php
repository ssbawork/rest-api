<?php

use App\Models\Favorite;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ProductsControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetProductsList(): void
    {
        $user     = User::factory()->create();
        Product::factory(5)
            ->has(Favorite::factory()->for($user)->count(1))
            ->has(Favorite::factory()->for(User::factory()->create())->count(1))
            ->create();

        Sanctum::actingAs(
            $user,
            ['*']
        );

        $response = $this->json('GET', '/api/products/');

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(5)
        );
    }

    /**
     *
     * @return void
     */
    public function testUploadProductImage(): void
    {
        $user     = User::factory()->create(['is_admin' => true]);

        $products = Product::factory()
            ->has(Favorite::factory()->for($user)->count(1))
            ->has(Favorite::factory()->for(User::factory()->create())->count(1))
            ->create();

        Sanctum::actingAs(
            $user,
            ['*']
        );

        $image = UploadedFile::fake()->image('blog.webp', 375, 375);

        $response = $this->json('POST', '/api/products/'.$products->slug.'/images', [
            'image' => $image,
            'image_alt' => $this->faker->word,
            'main' => false
        ]);

        $response->assertCreated();
    }
}

