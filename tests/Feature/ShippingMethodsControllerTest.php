<?php

use App\Models\ShippingMethod;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ShippingMethodsControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetShippingMethodsList(): void
    {
        $paymentMethods = ShippingMethod::factory(2)->create();

        $response = $this->json('GET', '/api/shipping/');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(2)
                 ->first(fn ($json) =>
                    $json->where('id', $paymentMethods[0]['id'])
                        ->where('name', $paymentMethods[0]['name'])
                        ->where('slug', $paymentMethods[0]['slug'])
                        ->etc()
                )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetShippingMethodSingle(): void
    {
        $paymentMethod = ShippingMethod::factory()->create();

        $response = $this->json('GET', '/api/shipping/'.$paymentMethod['slug'] );

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $paymentMethod['name'])
                 ->where('slug', $paymentMethod['slug'])
                 ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostShippingMethodCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/shipping/', [
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->word,
            'desc' => $this->faker->text,
            'time' => rand(10,20),
            'cost' => rand(5,99),
            'cost_per_piece' => $this->faker->boolean(90),
            'active' => true
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPutShippingMethodEditOk(): void
    {
        $paymentMethod = ShippingMethod::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $name = $this->faker->word;
        $slug = $this->faker->word;

        $response = $this->json('PUT', '/api/shipping/'.$paymentMethod['slug'], [
            'name' => $name,
            'slug' => $slug,
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', $name)
                ->where('slug', $slug)
                ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeleteShippingMethodDeleteOk(): void
    {
        $paymentMethod = ShippingMethod::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/shipping/'.$paymentMethod['slug']);

        $response->assertOk();
    }

}
