<?php

use App\Models\User;
use Database\Seeders\CountriesSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CountriesControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetCountriesList(): void
    {
        $this->seed(CountriesSeeder::class);

        $response = $this->json('GET', '/api/countries');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(249)
                 ->first(fn ($json) =>
                    $json->where('id', 1)
                        ->where('name', 'Afghanistan')
                        ->where('iso', 'AF')
                        ->etc()
                )
        );
    }

    /**
     *
     * @return void
     */
    public function testGetCountrySingle(): void
    {
        $this->seed(CountriesSeeder::class);

        $response = $this->json('GET', '/api/countries/af');

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('name', 'Afghanistan')
                 ->where('iso', 'AF')
                 ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostCountryCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/countries', [
            'name' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'iso' => 'XX',
            'delivery' => $this->faker->boolean()
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPostCountryCreateUnauthorized(): void
    {
        $response = $this->json('POST', '/api/countries', [
            'name' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'iso' => 'XX',
            'delivery' => $this->faker->boolean()
        ]);

        $response->assertUnauthorized();
    }

    /**
     *
     * @return void
     */
    public function testPostCountryCreateNonAdminUnauthorized(): void
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->json('POST', '/api/countries', [
            'name' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'iso' => 'XX',
            'delivery' => $this->faker->boolean()
        ]);

        $response->assertUnauthorized();
    }

    /**
     *
     * @return void
     */
    public function testPostCountryCreatePreconditionFailed(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/countries', []);

        $response->assertStatus(Response::HTTP_PRECONDITION_FAILED);
    }

    /**
     *
     * @return void
     */
    public function testPutCountryEditOk(): void
    {
        $this->seed(CountriesSeeder::class);

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $data = [
            'name' => $this->faker->word(),
            'active' => $this->faker->boolean(),
            'delivery' => $this->faker->boolean()
        ];

        $response = $this->json('PUT', '/api/countries/af', $data);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->where('name', $data['name'])
            ->where('active', $data['active'])
            ->where('delivery', $data['delivery'])
            ->where('iso', 'AF')
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPutCountryEditNonAdminUnauthorized(): void
    {
        Sanctum::actingAs(
            User::factory()->create([]),
            ['*']
        );

        $response = $this->json('PUT', '/api/countries/af', []);

        $response->assertUnauthorized();
    }

    /**
     *
     * @return void
     */
    public function testPutCountryEditUnauthorized(): void
    {
        Sanctum::actingAs(
            User::factory()->create([]),
            ['*']
        );

        $response = $this->json('PUT', '/api/countries/af', []);

        $response->assertUnauthorized();
    }

    /**
     *
     * @return void
     */
    public function testPutCountryEditWithEmptyBodyOk(): void
    {
        $this->seed(CountriesSeeder::class);

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('PUT', '/api/countries/af', []);

        $response->assertOk();
    }

    /**
     *
     * @return void
     */
    public function testDeleteCountryDeleteOk(): void
    {
        $this->seed(CountriesSeeder::class);

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/countries/af');

        $response->assertOk();
    }
}
