<?php

use App\Models\DiscountCode;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class DiscountCodesControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /**
     *
     * @return void
     */
    public function testGetDiscountCodesList(): void
    {
        DiscountCode::factory(2)->create();

        $response = $this->json('GET', '/api/discounts');

        $response->assertOk();

        $response->assertJson(fn (AssertableJson $json) =>
            $json->has(2)
        );
    }

    /**
     *
     * @return void
     */
    public function testGetDiscountCodeSingle(): void
    {
        $discount_code = DiscountCode::factory()->create();

        $response_id = $this->json('GET', '/api/discounts/'.$discount_code['id'] );

        $response_id->assertJson(fn (AssertableJson $json) =>
            $json->where('code', $discount_code['code'])
                 ->etc()
        );

        $response_code = $this->json('GET', '/api/discounts/'.$discount_code['code'] );

        $response_code->assertJson(fn (AssertableJson $json) =>
        $json->where('code', $discount_code['code'])
            ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testPostDiscountCodeCreateOk(): void
    {
        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('POST', '/api/discounts', [
            'code' => $this->faker->unique()->word,
            'count' => 15,
            'untill' => $this->faker->date,
            'percent' => true,
            'value' => 10,
            'active' => true,
        ]);

        $response->assertCreated();
    }

    /**
     *
     * @return void
     */
    public function testPutDiscountCodeEditOk(): void
    {
        $discount_code = DiscountCode::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $code = $this->faker->unique()->word;

        $response = $this->json('PUT', '/api/discounts/'.$discount_code['id'], [
            'code' => $code,
        ]);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('code', $code)
                ->etc()
        );
    }

    /**
     *
     * @return void
     */
    public function testDeleteDiscountCodeDeleteOk(): void
    {
        $discount_code = DiscountCode::factory()->create();

        Sanctum::actingAs(
            User::factory()->create(['is_admin' => True]),
            ['*']
        );

        $response = $this->json('DELETE', '/api/discounts/'.$discount_code['id']);

        $response->assertOk();
    }
}
