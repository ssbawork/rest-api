<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\DiscountCodesController;
use App\Http\Controllers\FavoritesController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\PaymentMethodsController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\QAController;
use App\Http\Controllers\ReviewsController;
use App\Http\Controllers\ShippingMethodsController;
use App\Http\Controllers\StatsController;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegistrationController;
use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/xsrf', [CsrfCookieController::class, 'get'])->name('xsrf'); //TODO Comment and test

Route::post('/login',[LoginController::class, 'Login'])->name('login');
Route::post('/signup',[RegistrationController::class, 'SignUp'])->name('signup');
Route::put('/recovery',[RegistrationController::class, 'SendPasswordResetEmail'])->name('sendpasswordresetemail');
Route::post('/recovery',[RegistrationController::class, 'SetNewPassword'])->name('setnewpassword');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout',[LoginController::class, 'Logout'])->name('logout');
});

Route::get('/countries', [CountriesController::class, 'list'])->name('countries.list');
Route::get('/countries/{country_iso}', [CountriesController::class, 'single'])->name('countries.single');
Route::group(['middleware' => ['auth:sanctum','admin']], function () {
    Route::post('/countries',[CountriesController::class, 'create'])->name('countries.create');
    Route::put('/countries/{country_iso}', [CountriesController::class, 'edit'])->name('countries.edit');
    Route::delete('/countries/{country_iso}', [CountriesController::class, 'delete'])->name('countries.delete');
});

Route::prefix('blog')->group(function () {
    Route::get('/articles', [BlogController::class, 'list'])->name('blog.list');
    Route::get('/articles/{id}', [BlogController::class, 'single'])->name('blog.single');
    Route::get('/categories', [BlogController::class, 'categories'])->name('blog.categories');
    Route::get('/categories/{id}', [BlogController::class, 'category'])->name('blog.category');
    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::post('/articles', [BlogController::class, 'create'])->name('blog.create');
        Route::put('/articles/{id}', [BlogController::class, 'edit'])->name('blog.edit');
        Route::delete('/articles/{id}', [BlogController::class, 'delete'])->name('blog.delete');
        Route::post('/categories', [BlogController::class, 'category_create'])->name('blog.category.create');
        Route::put('/categories/{id}', [BlogController::class, 'category_edit'])->name('blog.category.edit');
        Route::delete('/categories/{id}', [BlogController::class, 'category_delete'])->name('blog.category.delete');
    });
});

Route::prefix('discounts')->group(function () {
    Route::get('/', [DiscountCodesController::class, 'list'])->name('discount.list');
    Route::get('/{code_or_id}', [DiscountCodesController::class, 'single'])->name('discount.single');
    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::post('/', [DiscountCodesController::class, 'create'])->name('discount.create');
        Route::put('/{id}', [DiscountCodesController::class, 'edit'])->name('discount.edit');
        Route::delete('/{id}', [DiscountCodesController::class, 'delete'])->name('discount.delete');
    });
});

Route::prefix('payments')->group(function () {
    Route::get('/', [PaymentMethodsController::class, 'list'])->name('payments.list');
    Route::get('/{slug}', [PaymentMethodsController::class, 'single'])->name('payments.single');
    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::post('/', [PaymentMethodsController::class, 'create'])->name('payments.create');
        Route::put('/{slug}', [PaymentMethodsController::class, 'edit'])->name('payments.edit');
        Route::delete('/{slug}', [PaymentMethodsController::class, 'delete'])->name('payments.delete');
    });
});

Route::prefix('shipping')->group(function () {
    Route::get('/', [ShippingMethodsController::class, 'list'])->name('shipping.list');
    Route::get('/{slug}', [ShippingMethodsController::class, 'single'])->name('shipping.single');
    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::post('/', [ShippingMethodsController::class, 'create'])->name('shipping.create');
        Route::put('/{slug}', [ShippingMethodsController::class, 'edit'])->name('shipping.edit');
        Route::delete('/{slug}', [ShippingMethodsController::class, 'delete'])->name('shipping.delete');
    });
});

Route::prefix('order')->group(function () {
    Route::post('/', [OrdersController::class, 'create'])->name('order.create');
    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('/history', [OrdersController::class, 'history'])->name('order.history');
        Route::get('/history/{code}', [OrdersController::class, 'singleByCode'])->name('order.bycode');
    });
    Route::get('/a/{hash}', [OrdersController::class, 'singleByHash'])->name('order.byhash');
});

Route::prefix('orders')->group(function () {
    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::get('/', [OrdersController::class, 'list'])->name('order.list');
        Route::get('/{id}', [OrdersController::class, 'single'])->name('order.single');
        Route::put('/{id}', [OrdersController::class, 'edit'])->name('order.edit');
        Route::delete('/{id}', [OrdersController::class, 'delete'])->name('order.delete');
    });
});

Route::get('/bestsellings',[ProductsController::class, 'bestsellings'])->name('products.bestsellings');
Route::get('/recommends/',[ProductsController::class, 'recommends'])->name('products.recommends');
Route::prefix('products')->group(function () {
    Route::get('/',[ProductsController::class, 'list'])->name('products.list');
    Route::get('/{productCode}',[ProductsController::class, 'single'])->name('products.single')->whereAlphaNumeric('productCode');
    Route::get('/{productCode}/comments',[ProductsController::class, 'comments'])->name('products.comments')->whereAlphaNumeric('productCode');
    Route::get('/{productCode}/reviews',[ProductsController::class, 'reviews'])->name('products.reviews')->whereAlphaNumeric('productCode');
    Route::get('/{productCode}/qa',[ProductsController::class, 'qa'])->name('products.qa')->whereAlphaNumeric('productCode');

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::post('/{productCode}/comments',[CommentsController::class, 'create'])->name('comments.create')->whereAlphaNumeric('productCode');
        Route::put('/{productCode}/comments/{id}',[CommentsController::class, 'edit'])->name('comments.edit')->whereAlphaNumeric('productCode');
        Route::delete('/{productCode}/comments/{id}',[CommentsController::class, 'delete'])->name('comments.delete')->whereAlphaNumeric('productCode');

        Route::post('/{productCode}/reviews',[ReviewsController::class, 'create'])->name('reviews.create')->whereAlphaNumeric('productCode');
        Route::put('/{productCode}/review/{id}',[ReviewsController::class, 'edit'])->name('reviews.edit')->whereAlphaNumeric('productCode');
        Route::delete('/{productCode}/review/{id}',[ReviewsController::class, 'delete'])->name('reviews.delete')->whereAlphaNumeric('productCode');
    });

    Route::group(['middleware' => ['auth:sanctum','admin']], function () {
        Route::post('/{productCode}/images',[ProductsController::class, 'uploadImages'])->name('products.uploadImages')->whereAlphaNumeric('productCode');
        Route::delete('/{productCode}/images',[ProductsController::class, 'deleteImages'])->name('products.deleteImages')->whereAlphaNumeric('productCode');
        Route::post('/{productCode}/qa',[QAController::class, 'create'])->name('qa.create')->whereAlphaNumeric('productCode');
        Route::put('/{productCode}/qa/{id}',[QAController::class, 'edit'])->name('qa.edit')->whereAlphaNumeric('productCode');
        Route::delete('/{productCode}/qa/{id}',[QAController::class, 'delete'])->name('qa.delete')->whereAlphaNumeric('productCode');
    });

});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/favorites/}',[FavoritesController::class, 'create'])->name('favorites.create')->whereAlphaNumeric('productCode');
    Route::delete('/favorites/{productCode}',[FavoritesController::class, 'delete'])->name('favorites.delete')->whereAlphaNumeric('productCode');
});

Route::get('/ua',[StatsController::class, 'create'])->name('stats.create');


Route::get('/frontend/614794eb7c75dd163997797d253fb26e/release',[FrontEndController::class, 'release'])->name('frontend.release');

