<?php

namespace Database\Seeders;

use App\Models\DiscountCode;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\ShippingMethod;
use App\Models\User;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::factory()
            ->count( 2 )
            ->has(Product::factory()->count(5))
            ->for(User::factory()->create())
            ->for(DiscountCode::factory()->create())
            ->for(ShippingMethod::factory()->create())
            ->for(PaymentMethod::factory()->create())
            ->create();
    }
}
