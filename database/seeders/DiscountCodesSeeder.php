<?php

namespace Database\Seeders;

use App\Models\DiscountCode;
use Illuminate\Database\Seeder;

class DiscountCodesSeeder extends Seeder
{
    const CODES_COUNT           = 4;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DiscountCode::factory()->count(self::CODES_COUNT)->create();
    }
}
