<?php

namespace Database\Seeders;

use App\Models\BlogCategory;
use App\Models\Blog;
use App\Models\Image;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BlogsSeeder extends Seeder
{
    /**
     * The current Faker instance.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogCategories = BlogCategory::factory()->count(2)->create();

        foreach ($blogCategories as $blogCategory){
            $article_count = 5;

            for($i = 0; $i < $article_count; $i++){

                $image = Image::factory()->create();
                $name = Str::slug($image->alt) . '.webp';

                $target = Storage::path('images' . DIRECTORY_SEPARATOR . 'blogs' . DIRECTORY_SEPARATOR . $name);
                $path = storage_path('assets' . DIRECTORY_SEPARATOR . 'blog375x375.webp');
                File::copy($path, $target);

                $image->name = $name;
                $image->main = true;

                $image->save();

                Blog::factory()->state([
                    'cat_id' => $blogCategory->id,
                    'image_id' => $image->id,
                    'article_type' => "ArticleType"
                ])->create();
            }
        }
    }
}
