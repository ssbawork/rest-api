<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesSeeder::class);
        $this->call(DiscountCodesSeeder::class);
        $this->call(BrandsSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(BlogsSeeder::class);
        $this->call(ProductsSeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(StatsSeeder::class);
    }
}
