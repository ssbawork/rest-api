<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Comment;
use App\Models\Favorite;
use App\Models\Image;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductsImages;
use App\Models\QA;
use App\Models\Review;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    const PRODUCT_COUNT             = 10;
    const REVIEW_PER_PRODUCT        = 2;
    const ORDER_PER_PRODUCT         = 2;
    const FAVORITE_PER_PRODUCT      = 5;
    const COMMENTS_PER_PRODUCT      = 2;
    const QA_PER_PRODUCT            = 1;

    /**
     * The current Faker instance.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::factory(self::PRODUCT_COUNT)->state([
            'brand_id' => Brand::all()->random()->id
        ])->create();

        foreach ($products as $product){
            for($i = 0; $i <= self::REVIEW_PER_PRODUCT; $i++){
                Review::factory()
                    ->for($product)
                    ->for(User::factory()->create())
                    ->create();
            }

            for($i = 0; $i <= self::FAVORITE_PER_PRODUCT; $i++){
                Favorite::factory()
                    ->for($product)
                    ->for(User::factory()->create())
                    ->create();
            }

            for($i = 0; $i <= self::QA_PER_PRODUCT; $i++){
                QA::factory()
                    ->for($product)
                    ->for(User::factory()->create())
                    ->create();
            }

            for($i = 0; $i <= self::COMMENTS_PER_PRODUCT; $i++){
                Comment::factory()
                    ->for($product)
                    ->for(User::factory()->create())
                    ->create();
            }
        }
    }
}
