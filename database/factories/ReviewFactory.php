<?php

namespace Database\Factories;

use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stars' => rand(1, 5),
            'feelings' => $this->faker->sentence(12),
            'pros' => $this->faker->sentence(12),
            'cons' => $this->faker->sentence(12),
            'review_score' => rand(-99, 99),
            'status' => 'published'
        ];
    }
}
