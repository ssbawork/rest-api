<?php

namespace Database\Factories;

use App\Models\ProductsImages;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsImagesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductsImages::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }
}
