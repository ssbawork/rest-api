<?php

namespace Database\Factories;

use App\Models\Blog;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $langKey = ['en', 'fr', 'it'][rand(0,2)];

        $localizations = [
            "fr" => "fr_FR",
            "en" => "en_US",
            "it" => "it_IT",
        ];
        $this->locale = $localizations[$langKey];

        return [
            'name' => $this->faker->word,
            'image_id' => Image::factory()->create()->id,
            'slug' => $this->faker->unique()->word,
            'lang' => $langKey,
            'country' => $this->faker->country,
            'data' => $this->faker->paragraph,
            'view' => rand(55, 2500),
            'score'=> 5,
            'type' => ['article', 'howto', 'page', 'tips'][rand(0,2)],
        ];
    }
}
