<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Image;
use App\Models\Product;
use App\Models\ProductsImages;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    const IMAGE_PER_PRODUCT         = 2;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Product $product) {
            $images =  Image::factory()->count(self::IMAGE_PER_PRODUCT)->create();
            $main = false;
            foreach ($images as $image){

                $name = Str::slug($image->alt) . '.webp';

                $target = Storage::path('images' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . $name);
                $path = storage_path('assets' . DIRECTORY_SEPARATOR . 'products190x305.webp');
                File::copy($path, $target);

                $image->name = $name;

                if(!$main){
                    $image->main = true;
                    $main = true;
                }

                $image->save();
                ProductsImages::factory()->state([
                    'image_id' => $image->id,
                    'product_id' => $product->id
                ])->create();
            }
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'brand_id' => Brand::factory()->create()->id,
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->word,
            'type' => ['cigarettes', 'ths', 'sticks'][rand(0,2)],
            'tar' => rand(1, 100) / 10,
            'nicotine' => rand(1, 100) / 10,
            'color' => ['white', 'black'][rand(0,1)],
            'price' => rand(100, 500) / 10,
            'discount' => rand(0, 50),
            'views' => rand(0, 50),
            'featured' => $this->faker->boolean,
            'min_count' => 2
        ];
    }
}
