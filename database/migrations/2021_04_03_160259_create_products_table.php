<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('brand_id')->constrained('brands')->onDelete('restrict');
            $table->string('name', 255);
            $table->string('slug', 255)->unique();
            $table->enum('type', ['cigarettes', 'ths', 'sticks']);
            $table->float('tar', 4, 2);
            $table->float('nicotine', 4, 2);
            $table->enum('color', ['white', 'black']);
            $table->float('price', 8, 2);
            $table->integer('views');
            $table->integer('min_count');
            $table->boolean('featured')->default(false)->comment("Featured Products for FeaturedComponent");
            $table->unsignedSmallInteger('discount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
