<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'hash',
        'user_id',
        'discount_id',
        'shipper_id',
        'payment_method_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'zip',
        'city',
        'state',
        'country',
        'shipping_cost',
        'discount',
        'total',
        'status'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saved(function ($order) {
            $order->discountCode->count -= 1;
            $order->discountCode->save();
        });
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'orders__products', 'order_id', 'product_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function discountCode(): BelongsTo
    {
        return $this->belongsTo(DiscountCode::class, 'discount_id', 'id');
    }

    public function shippingMethod(): BelongsTo
    {
        return $this->belongsTo(ShippingMethod::class, 'shipper_id', 'id');
    }

    public function paymentMethod(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id', 'id');
    }

    public function update(array $attributes = [], array $options = [])
    {
        // calculate discount and total REDEEM DISCOUNT

        parent::update($attributes, $options);
    }

    public static function create($attributes)
    {
        //  REDEEM DISCOUNT код ГЕНЕРАТОР
        $attributes['code'] = self::getUniqueCode();

        return (new static)->newQuery()->create($attributes);
    }

    public static function getUniqueCode(){
        $code  = strtoupper(Str::random(6));
        $order = Order::where('code', $code)->count();

        if($order > 0){
            return self::getUniqueCode();
        }

        return $code;
    }
}
