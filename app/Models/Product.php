<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $appends = ['rate'];

    protected $casts = [
        'tar' => 'decimal:2',
        'nicotine' => 'decimal:2',
        'price' => 'decimal:2',
        'discount' => 'int',
        'featured' => 'bool'
    ];

    public function image()
    {
        return $this->hasOneThrough(
            Image::class,
            ProductsImages::class,
            'product_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...
            'id', // Local key on the projects table...
            'id' // Local key on the environments table...
        )->where('main', 1);
    }

    public function images()
    {
        return $this->hasManyThrough(
            Image::class,
            ProductsImages::class,
            'product_id', // Foreign key on the environments table...
            'id', // Foreign key on the deployments table...
            'id', // Local key on the projects table...
            'id' // Local key on the environments table...
        );
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->where('status', 'published');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class)->where('status', 'published');
    }

    public function qas()
    {
        return $this->hasMany(QA::class)->where('status', 'published');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function getRateAttribute()
    {
        $avg = Review::where('product_id', $this->id)->avg('stars');

        return $avg;
    }

}
