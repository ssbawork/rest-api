<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JeroenG\Explorer\Application\Explored;
use Laravel\Scout\Searchable;

class Blog extends Model implements Explored
{
    use HasFactory, Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cat_id',
        'image_id',
        'name',
        'slug',
        'view',
        'score',
        'lang',
        'country',
        'data',
        'type',
        'article_type',
    ];

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'cat_id', 'id');
    }

    public function mappableAs(): array
    {
        return [
            'id' => 'keyword',
            'category_id' => 'integer',
            'name' => 'text',
            'slug' => 'text',
            'view' => 'integer',
            'score' => 'integer',
            'lang' => 'text',
            'country' => 'text',
            'data' => 'text',
            'type' => 'text',
            'article_type' => 'text'
        ];
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'category_id' => $this->cat_id,
            'name' => $this->name,
            'slug' => $this->slug,
            'view' => $this->view,
            'score' => $this->score,
            'lang' => $this->lang,
            'country' => $this->country,
            'data' => $this->data,
            'type' => $this->type,
            'article_type' => $this->article_type,
        ];
    }
}
