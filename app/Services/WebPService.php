<?php

namespace App\Services;

use App\Models\Image;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Intervention;

class WebPService {


    /**
     * Convert Image to WebP
     *
     * @param string $name
     * @param int $width Image width
     * @param int $height Image height
     * @param string $path Internal path of Image
     * @param int $quality Image quality
     *
     * @return string
     */
    public static function convert(string $name, int $width, int $height, string $path, int $quality = 90): string
    {
        $img = Intervention::make($path)->encode('webp', $quality);
        $full_path = Storage::path('images' . DIRECTORY_SEPARATOR . 'blogs' . DIRECTORY_SEPARATOR . $name . '.webp');

        $img->resize($width, $height, function ($const) {
          $const->aspectRatio();
        })->save($full_path, $quality);

        return $name . '.webp';
    }
}
