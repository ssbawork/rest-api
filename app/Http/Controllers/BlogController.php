<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blog\BlogCategoriesRequest;
use App\Http\Requests\Blog\BlogCategoryCreateRequest;
use App\Http\Requests\Blog\BlogCategoryEditRequest;
use App\Http\Requests\Blog\BlogCreateRequest;
use App\Http\Requests\Blog\BlogEditRequest;
use App\Http\Requests\Blog\BlogsRequest;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Image;
use App\Services\WebPService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    /**
     * @param BlogsRequest $request
     *
     * @return JsonResponse
     */
    public function list(BlogsRequest $request): JsonResponse
    {
        $blogs = Blog::with('image');

        if($request->category_id){
            $blogs = $blogs->where("cat_id", $request->category_id);
        }

        if($request->name){
            $blogs = $blogs->whereLike("name", $request->name);
        }

        if($request->slug){
            $blogs = $blogs->whereLike("slug", $request->slug);
        }

        if($request->lang){
            $blogs = $blogs->where("lang", $request->lang);
        }

        if($request->country){
            $blogs = $blogs->where("country", $request->country);
        }

        if($request->type){
            $blogs = $blogs->where("type", $request->type);
        }

        if($request->article_type){
            $blogs = $blogs->where("article_type", $request->article_type);
        }

        return response()->json($blogs->get());
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function single(Request $request, string $id): JsonResponse
    {
        $blog = Blog::with('image')->where('id', $id)->firstOrFail();

        return response()->json($blog);
    }

    /**
     * @param BlogCreateRequest $request
     *
     * @return JsonResponse
     */
    public function create(BlogCreateRequest $request): JsonResponse
    {
        $image = $request->image;
        $name = WebPService::convert($request->image_alt,375,375, $image->path());

        $image = Image::create([
            'name' => $name,
            'alt'  => $request->image_alt,
            'main' => true,
        ]);

        $blog = Blog::create([
            'cat_id'        => $request->category_id,
            'image_id'      => $image->id,
            'name'          => $request->name,
            'slug'          => $request->slug,
            'view'          => 0,
            'score'         => 0,
            'lang'          => $request->lang,
            'country'       => $request->country,
            'data'          => $request->data,
            'type'          => $request->type,
            'article_type'  => $request->article_type
        ]);

        return response()->json(Blog::with('image')->where('id', $blog->id)->firstOrFail(), Response::HTTP_CREATED);
    }

    /**
     * @param BlogEditRequest $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function edit(BlogEditRequest $request, string $id): JsonResponse
    {
        $input = $request->validated();

        $blog = Blog::where('id', $id)->firstOrFail();

        $blog->update($input);

        if($request->image){
            $image = $request->image;
            $name = WebPService::convert(375,375, $image->path());

            $current_image = Image::where('id', $blog->image_id)->firstOrFail();

            Storage::delete('images' . DIRECTORY_SEPARATOR . 'blogs' . $current_image['name']);

            $current_image['name'] = $name;

            if($request->image_alt){
                $current_image['alt'] = $request->image_alt;
            }

            $current_image->save();
        }

        return response()->json(Blog::with('image')->where('id', $blog->id)->firstOrFail(), Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(Request $request, string $id): JsonResponse
    {
        $blog = Blog::where('id', $id)->firstOrFail();

        $blog->delete();

        return response()->json([],Response::HTTP_OK);
    }

    /**
     * @param BlogCategoriesRequest $request
     *
     * @return JsonResponse
     */
    public function categories(BlogCategoriesRequest $request): JsonResponse
    {
        $blog_categories = new BlogCategory();

        if($request->name){
            $blog_categories = $blog_categories->whereLike("name", $request->name);
        }

        if($request->slug){
            $blog_categories = $blog_categories->whereLike("slug", $request->slug);
        }

        if($request->desc){
            $blog_categories = $blog_categories->whereLike("lang", $request->desc);
        }

        if($request->active){
            $blog_categories = $blog_categories->where("active", $request->active);
        }

        return response()->json($blog_categories->get());
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function category(Request $request, string $id): JsonResponse
    {
        $category = BlogCategory::with('blogs')->where('id', $id)->firstOrFail();

        return response()->json($category);
    }

    /**
     * @param BlogCategoryCreateRequest $request
     *
     * @return JsonResponse
     */
    public function category_create(BlogCategoryCreateRequest $request): JsonResponse
    {
        $category = BlogCategory::create([
            'name'   => $request->name,
            'slug'   => $request->slug,
            'desc'   => $request->desc,
            'active' => $request->active
        ]);

        return response()->json($category, Response::HTTP_CREATED);
    }

    /**
     * @param BlogCategoryEditRequest $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function category_edit(BlogCategoryEditRequest $request, string $id): JsonResponse
    {
        $input = $request->validated();

        $category = BlogCategory::where('id', $id)->firstOrFail();

        $category->update($input);

        return response()->json($category, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function category_delete(Request $request, string $id): JsonResponse
    {
        $category = BlogCategory::where('id', $id)->firstOrFail();

        $category->delete();

        return response()->json([],Response::HTTP_OK);
    }




}
