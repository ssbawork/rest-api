<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\Brand;
use App\Models\Product;
use DOMDocument;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use ZipArchive;

class FrontendController extends Controller
{
    /**
     * @param Request $request
     *
     * @return BinaryFileResponse
     * @throws Exception
     * @throws \Throwable
     */
    public function release(Request $request)
    {
        try {
            $zip = new ZipArchive();
            $file_name = 'release' . date('dmy') . '.zip';

            if(file_exists(public_path($file_name))){
                unlink(public_path($file_name));
            }

            if ($zip->open(public_path($file_name), ZipArchive::CREATE) == TRUE) {

                $zip = $this->images($zip);
                $zip = $this->sitemap($zip, 'sitemap.xml');
                $zip = $this->robots($zip, 'robots.txt');
                $zip = $this->routes($zip, 'routes.txt');

                $zip->close();
            }else{
                throw new FileNotFoundException();
            }

            return response()->download(public_path($file_name));
        }catch (\Throwable $e){
            throw new $e;
        }
    }

    /**
     * @param ZipArchive $zip
     * @param string $path
     * @return ZipArchive
     *
     * @throws Exception
     */
    private function robots(ZipArchive $zip, string $path): ZipArchive
    {
        $front_end_url = env('FRONTEND_URL');

        if (is_null($front_end_url)) {
            throw new Exception("Invalid front_end_url");
        }

        if (is_null($path)) {
            throw new Exception("Invalid sitemap path");
        }

        $content =  "User-agent: *\n";
        $content .= "Allow: /\n";
        $content .= "\n";
        $content .= sprintf("Sitemap: %s/sitemap.xml\n", $front_end_url);

        Storage::put($path, $content);
        $zip->addFile(Storage::path($path), $path);

        return $zip;
    }

    /**
     * @param ZipArchive $zip
     * @param string $path
     * @return ZipArchive
     *
     * @throws Exception
     */
    private function sitemap(ZipArchive $zip, string $path): ZipArchive
    {

        $front_end_url = env('FRONTEND_URL');

        if (is_null($front_end_url)) {
            throw new Exception("Invalid front_end_url");
        }

        if (is_null($path)) {
            throw new Exception("Invalid sitemap path");
        }

        $locs = [
            "/" => '0.5',
            "/login/" => '0.5',
            "/shop/" => '0.5',
            "/cart/" => '0.5',
            "/faq/" => '0.5',
            "/bulk/" => '0.5',
            "/delivery/" => '0.5',
            "/contactus/" => '0.5',
            "/who-we-are/" => '0.5',
            "/blogs/" => '0.75',
        ];

        $brands = Brand::with("products")->where("active", true)->get();
        foreach ($brands as $brand){
            $loc = "/categories/". $brand->slug  ."/";
            $locs[$loc] = "0.75";
            foreach ($brand->products as $product){
                $loc = "/". $brand->slug ."/". $product->slug  ."/";
                $locs[$loc] = "1";
            }
        }

        $blog_categories = BlogCategory::with("blogs")->where("active", true)->get();
        foreach ($blog_categories as $category){
            $loc = "/blogs/". $category->slug  ."/";
            $locs[$loc] = "0.75";
            foreach ($category->blogs as $blog){
                $loc = "/blogs/". $category->slug  ."/". $blog->slug  ."/";
                $locs[$loc] = "0.75";
            }
        }

        $xml = new SimpleXMLElement('<urlset/>');
        $xml->addAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

        foreach ($locs as $loc => $priority){
            $url = $xml->addChild('url');
            $url->addChild('loc', $front_end_url . $loc);
            $url->addChild('lastmod',date('c',time()));
            $url->addChild('priority', $priority);
        }

        $xmlDocument = new DOMDocument('1.0');
        $xmlDocument->preserveWhiteSpace = false;
        $xmlDocument->formatOutput = true;
        $xmlDocument->loadXML($xml->asXML());

        Storage::put($path, $xmlDocument->saveXML());
        $zip->addFile(Storage::path($path), $path);

        return $zip;
    }

    /**
     * @param ZipArchive $zip
     * @param string $path
     * @return ZipArchive
     *
     * @throws Exception
     */
    private function routes(ZipArchive $zip, string $path): ZipArchive
    {
        $front_end_url = env('FRONTEND_URL');

        if (is_null($front_end_url)) {
            throw new Exception("Invalid front_end_url");
        }

        if (is_null($path)) {
            throw new Exception("Invalid sitemap path");
        }

        $generated_routes = [];

        $brands = Brand::with("products")->where("active", true)->get();
        foreach ($brands as $brand){
            foreach ($brand->products as $product){
                $generated_routes[] = "/". $brand->slug ."/". $product->slug  ."/";
            }
        }

        $categories = BlogCategory::with("blogs")->where("active", true)->get();
        foreach ($categories as $category){
            foreach ($category->blogs as $blog){
                $generated_routes[] = "/blogs/". $category->slug  ."/". $blog->slug  ."/";
            }
        }

        $content = implode("\n", $generated_routes);
        Storage::put($path, $content);
        $zip->addFile(Storage::path($path), $path);

        return $zip;
    }

    /**
     * @param ZipArchive $zip
     * @return ZipArchive
     *
     * @throws Exception
     */
    private function images(ZipArchive $zip): ZipArchive
    {
        $pathes = [
            'images' . DIRECTORY_SEPARATOR . 'blogs' . DIRECTORY_SEPARATOR,
            'images' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR,
        ];

        foreach ($pathes as $path){
            $full_path = Storage::path($path);
            $files = File::glob($full_path . '*.webp');

            foreach ($files as $file){
                $zip->addFile($file, $path . basename($file));
            }
        }

        return $zip;
    }

}
