<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SendPasswordResetEmailRequest;
use App\Http\Requests\Auth\SetNewPasswordRequest;
use App\Http\Requests\Auth\SignUpRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends Controller
{
    /**
     * User Registration
     *
     * @param SignUpRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function SignUp(SignUpRequest $request): JsonResponse
    {
        $user = User::create([
            'email'    => $request->email,
            'name'     => $request->name,
            'password' => Hash::make($request->password),
        ]);

        $token = $user->createToken($request->token_name);

        //TODO Send Registration Email

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user,
            'token' => $token->plainTextToken
        ], 201);
    }

    /**
     * Send email to user for confirm password restoration request.
     *
     * @param SendPasswordResetEmailRequest $request
     * @return JsonResponse
     */
    public function SendPasswordResetEmail(SendPasswordResetEmailRequest $request): JsonResponse
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );

        if($status === Password::RESET_LINK_SENT)
            return response()->json(['message' => 'Email successfully sent'], Response::HTTP_OK);


        return response()->json(['message' => 'Exceptions while sending email'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Set new password.
     *
     * @param SetNewPasswordRequest $request
     * @return JsonResponse
     */
    public function SetNewPassword(SetNewPasswordRequest $request): JsonResponse
    {

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ]);
                $user->save();
            }
        );

        if($status === Password::PASSWORD_RESET)
            return response()->json(['message' => 'User password successfully reset'], Response::HTTP_OK);

        $message = "Error while password resetting";
        $code    = Response::HTTP_BAD_REQUEST;
        switch ($status){
            case Password::INVALID_TOKEN:
                $code    = Response::HTTP_UNAUTHORIZED;
                break;
            case Password::INVALID_USER:
                $code    = Response::HTTP_NOT_FOUND;
                break;
        }

        return response()->json(['message' => $message],$code);
    }
}
