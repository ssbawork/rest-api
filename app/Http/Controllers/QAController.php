<?php

namespace App\Http\Controllers;

use App\Http\Requests\QA\QACreateRequest;
use App\Http\Requests\QA\QAEditRequest;
use App\Models\QA;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QAController extends Controller
{
    /**
     * Create qa.
     *
     * @param QACreateRequest $request
     * @param string $productCode
     * @return JsonResponse
     */
    public function create(QACreateRequest $request, string $productCode): JsonResponse
    {
        $product = Product::where('slug', $productCode)->firstOrFail();

        $qa = QA::create([
            'product_id' => $product->user_id,
            'user_id' => $product->user_id,
            'question' => $request->question,
            'answer' => $request->answer,
            'status' => $request->status
        ]);

        return response()->json($qa, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param QAEditRequest $request
     * @param string $productCode
     * @param string $id
     * @return JsonResponse
     */
    public function edit(QAEditRequest $request, string $productCode, string $id): JsonResponse
    {
        $input = [];
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $qa = QA::where('id', $id)->where('product_id', $product->id)->firstOrFail();

        $qa->update($input);

        return response()->json($qa);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $productCode
     * @param string $id
     * @return JsonResponse
     */
    public function delete(Request $request, string $productCode, string $id): JsonResponse
    {
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $qa = QA::where('id', $id)->where('product_id', $product->id)->firstOrFail();

        $qa->delete();

        return response()->json([],Response::HTTP_OK);
    }
}
