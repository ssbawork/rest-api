<?php

namespace App\Http\Controllers;

use App\Http\Requests\Comments\CommentCreateRequest;
use App\Http\Requests\Comments\CommentEditRequest;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentsController extends Controller
{
    /**
     * Create comment.
     *
     * @param CommentCreateRequest $request
     * @param string $productCode
     * @return JsonResponse
     */
    public function create(CommentCreateRequest $request, string $productCode): JsonResponse
    {
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();

        $user_id = $user->id;
        $status = 'created';

        if($user->is_admin){
            $user_id = $request->user_id ?? $user->id;
            $status = $request->status ?? $status;
        }

        if(is_null($user_id)){
            return response()->json([], Response::HTTP_NOT_FOUND);
        }

        $comment = Comment::create([
            'product_id' => $product->id,
            'user_id' => $user_id,
            'comment' => $request->comment,
            'status' => $status
        ]);

        return response()->json($comment, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param CommentEditRequest $request
     * @return JsonResponse
     */
    public function edit(CommentEditRequest $request, string $productCode, string $comment_id): JsonResponse
    {
        $input = [];
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $comment = Comment::where('id', $comment_id)->where('product_id', $product->id)->firstOrFail();

        if(!is_null($request->comment) ){
            $input['comment'] = $request->comment;
        }

        if($user->is_admin && !is_null($request->status) ){
            $input['status'] = $request->status;
        }

        $comment->update($input);

        return response()->json($comment);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $productCode
     * @param string $comment_id
     * @return JsonResponse
     */
    public function delete(Request $request, string $productCode, string $comment_id): JsonResponse
    {
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $comment = Comment::where('id', $comment_id)->where('product_id', $product->id)->firstOrFail();

        if($user->is_admin || $user->is_admin == $comment->id){
            $comment->delete();
        }

        return response()->json([],Response::HTTP_OK);
    }
}
