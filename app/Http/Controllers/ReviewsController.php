<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reviews\ReviewCreateRequest;
use App\Http\Requests\Reviews\ReviewEditRequest;
use App\Models\Country;
use App\Models\Review;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ReviewsController extends Controller
{
    /**
     * Create review.
     *
     * @param ReviewCreateRequest $request
     * @param string $productCode
     * @return JsonResponse
     */
    public function create(ReviewCreateRequest $request, string $productCode): JsonResponse
    {
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();

        $user_id = $user->id;
        $status = 'created';

        if($user->is_admin){
            $user_id = $request->user_id ?? $user->id;
            $review_score = $request->review_score ?? 0;
            $status = $request->status ?? $status;
        }

        if(is_null($user_id)){
            return response()->json([], Response::HTTP_NOT_FOUND);
        }

        $review = Review::create([
            'product_id' => $product->id,
            'user_id' => $user_id,
            'stars' => $request->stars,
            'feelings' => $request->feelings,
            'pros' => $request->pros,
            'cons' => $request->cons,
            'review_score' => $review_score,
            'status' => $status
        ]);

        return response()->json($review, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param ReviewEditRequest $request
     * @param string $productCode
     * @param string $review_id
     * @return JsonResponse
     */
    public function edit(ReviewEditRequest $request, string $productCode, string $review_id): JsonResponse
    {
        $input = $request->validated();
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $review = Review::where('id', $review_id)->where('product_id', $product->id)->firstOrFail();

        if(!$user->is_admin){
            unset($input['status']);
        }

        $review->update($input);

        return response()->json($review);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $productCode
     * @param string $review_id
     * @return JsonResponse
     */
    public function delete(Request $request, string $productCode, string $review_id): JsonResponse
    {
        $user = auth('sanctum')->user();
        $product = Product::where('slug', $productCode)->firstOrFail();
        $review = Review::where('id', $review_id)->where('product_id', $product->id)->firstOrFail();

        if($user->is_admin || $user->id == $review->user_id){
            $review->delete();
        }

        return response()->json([],Response::HTTP_OK);
    }
}
