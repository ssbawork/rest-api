<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingMethods\ShippingMethodCreateRequest;
use App\Http\Requests\ShippingMethods\ShippingMethodEditRequest;
use App\Http\Requests\ShippingMethods\ShippingMethodsRequest;
use App\Models\ShippingMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ShippingMethodsController extends Controller
{
    /**
     * Show shipping methods list.
     *
     * @param ShippingMethodsRequest $request
     * @return JsonResponse
     */
    public function list(ShippingMethodsRequest $request): JsonResponse
    {
        $shippingMethods = new ShippingMethod();

        if($request->name){
            $shippingMethods = $shippingMethods->whereLike('name', $request->name);
        }

        if($request->slug){
            $shippingMethods = $shippingMethods->whereLike('slug', $request->slug);
        }

        if($request->desc){
            $shippingMethods = $shippingMethods->whereLike('desc', $request->desc);
        }

        if($request->min_time){
            $shippingMethods = $shippingMethods->where("time", '<=', $request->min_time);
        }

        if($request->max_time){
            $shippingMethods = $shippingMethods->where("time", '>=', $request->max_time);
        }

        if($request->min_cost){
            $shippingMethods = $shippingMethods->where("cost", '<=', $request->min_cost);
        }

        if($request->max_cost){
            $shippingMethods = $shippingMethods->where("cost", '>=', $request->max_cost);
        }

        if($request->by_piece){
            $shippingMethods = $shippingMethods->where('by_piece', boolval($request->by_piece));
        }

        if($request->active){
            $shippingMethods = $shippingMethods->where('active', boolval($request->active));
        }

        return response()->json($shippingMethods->get());
    }

    /**
     * Show single shipping method.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function single(Request $request, string $slug): JsonResponse
    {
        $shippingMethod = ShippingMethod::where('slug', $slug)->first();

        if(is_null($shippingMethod)){
            return response()->json([],Response::HTTP_NOT_FOUND);
        }

        return response()->json($shippingMethod);
    }

    /**
     * Create shipping method.
     *
     * @param ShippingMethodCreateRequest $request
     * @return JsonResponse
     */
    public function create(ShippingMethodCreateRequest $request): JsonResponse
    {
        $shippingMethod = ShippingMethod::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'desc' => $request->desc,
            'time' => $request->time,
            'cost' => $request->cost,
            'cost_per_piece' => $request->cost_per_piece ?? false,
            'active' => $request->active ?? false,
        ]);

        return response()->json($shippingMethod, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param ShippingMethodEditRequest $request
     * @param string $slug
     * @return JsonResponse
     */
    public function edit(ShippingMethodEditRequest $request, string $slug): JsonResponse
    {
        $input = $request->validated();

        $shippingMethod = ShippingMethod::where('slug', $slug)->firstOrFail();

        $shippingMethod->update($input);

        return response()->json($shippingMethod);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function delete(Request $request, string $slug): JsonResponse
    {
        $shippingMethod = ShippingMethod::where('slug', $slug)->firstOrFail();

        $shippingMethod->delete();

        return response()->json([],Response::HTTP_OK);
    }
}
