<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\OrderCreateRequest;
use App\Http\Requests\Orders\OrderEditRequest;
use App\Http\Requests\Orders\OrdersRequest;
use App\Models\DiscountCode;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Product;
use App\Models\ShippingMethod;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class OrdersController extends Controller
{
    const RELATIONS = ['products','user','discountCode','shippingMethod','paymentMethod'];

    /**
     * @param OrdersRequest $request
     *
     * @return JsonResponse
     */
    public function list(OrdersRequest $request): JsonResponse
    {
        $orders = new Order();

        if($request->code){
            $orders = $orders->where("code", $request->code);
        }

        if($request->user_id){
            $user = User::where('id', $request->user_id)->firstOrFail();
            $orders = $orders->where("user_id", $user->id);
        }

        if($request->anon){
            $orders = $orders->whereNull("user_id");
        }

        if($request->discount_id){
            $discount = DiscountCode::where('id', $request->discount_id)->firstOrFail();
            $orders = $orders->where("discount", $discount->id);
        }

        if($request->shipper_id){
            $shipping = ShippingMethod::where('id', $request->shipper_id)->firstOrFail();
            $orders = $orders->where("value", $shipping->id);
        }

        if($request->payment_method_id){
            $payment = PaymentMethod::where('id', $request->payment_method_id)->firstOrFail();
            $orders = $orders->where("active", $payment->id);
        }

        if($request->total_min){
            $orders = $orders->where("total", '<=', $request->total_min);
        }

        if($request->total_max){
            $orders = $orders->where("total", '>=', $request->total_max);
        }

        if($request->email){
            $orders = $orders->where("email", $request->email);
        }

        if($request->phone){
            $orders = $orders->where("phone", $request->phone);
        }

        if($request->first_name){
            $orders = $orders->where("first_name", $request->first_name);
        }

        if($request->products){
            $product_ids = $request->products;
            $orders = $orders->with([
                'products' => function($query) use ($product_ids){
                    $query->whereIn('name', $product_ids);
            }]);
        }

        return response()->json($orders->with(self::RELATIONS)->get());
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function single(Request $request, string $id): JsonResponse
    {
        $order = Order::with(self::RELATIONS)->where('id', $id)->firstOrFail();

        return response()->json($order);
    }

    /**
     * Get user history
     *
     * @param OrdersRequest $request
     *
     * @return JsonResponse
     */
    public function history(OrdersRequest $request): JsonResponse
    {
        $user = auth('sanctum')->user();

        $orders = Order::where("user_id", $user->id);

        if($request->code){
            $orders = $orders->where("code", $request->code);
        }

        if($request->anon){
            $orders = $orders->whereNull("user_id");
        }

        if($request->discount_id){
            $discount = DiscountCode::where('id', $request->discount_id)->firstOrFail();
            $orders = $orders->where("discount", $discount->id);
        }

        if($request->shipper_id){
            $shipping = ShippingMethod::where('id', $request->shipper_id)->firstOrFail();
            $orders = $orders->where("value", $shipping->id);
        }

        if($request->payment_method_id){
            $payment = PaymentMethod::where('id', $request->payment_method_id)->firstOrFail();
            $orders = $orders->where("active", $payment->id);
        }

        if($request->total_min){
            $orders = $orders->where("total", '<=', $request->total_min);
        }

        if($request->total_max){
            $orders = $orders->where("total", '>=', $request->total_max);
        }

        if($request->email){
            $orders = $orders->where("email", $request->email);
        }

        if($request->phone){
            $orders = $orders->where("phone", $request->phone);
        }

        if($request->first_name){
            $orders = $orders->where("first_name", $request->first_name);
        }

        if($request->products){
            $product_ids = $request->products;
            $orders = $orders->with([
                'products' => function($query) use ($product_ids){
                    $query->whereIn('name', $product_ids);
                }]);
        }

        return response()->json($orders->with(self::RELATIONS)->get());
    }


    /**
     * @param Request $request
     *
     * @param string $code
     * @return JsonResponse
     */
    public function singleByCode(Request $request, string $code): JsonResponse
    {
        $user = auth('sanctum')->user();

        $order = Order::where("user_id", $user->id)->with(self::RELATIONS)->where('code', $code)->firstOrFail();

        return response()->json($order);
    }

    /**
     * @param Request $request
     *
     * @param string $code
     * @return JsonResponse
     */
    public function singleByHash(Request $request, string $hash): JsonResponse
    {
        $order = Order::with(self::RELATIONS)->where(DB::raw('SHA2(code, 256)'), $hash)->firstOrFail();
        return response()->json($order);
    }

    /**
     * @param OrderEditRequest $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function edit(OrderEditRequest $request, string $id): JsonResponse
    {
        $input = $request->validated();
        $order = Order::where('id', $id)->firstOrFail();

        if($request->user_id){
            $user = User::where('id', $request->user_id)->firstOrFail();
            $input->user_id = $user->id;
        }

        if($request->anon){
            $input->user_id = null;
        }

        if($request->shipper_id){
            $shipping = ShippingMethod::where('id', $request->shipper_id)->firstOrFail();
            $input->shipper_id = $shipping->id;
            $input->shipping_cost = $shipping->cost;
        }

        if($request->payment_method_id){
            $payment = PaymentMethod::where('id', $request->payment_method_id)->firstOrFail();
            $input->payment_method_id = $payment->id;
        }

        if($request->products){
            $products = [];
            foreach ($input->products as $product){
                Product::where('id',$request->$product['id'])->firstOrFail();

                $products[] = [
                    $product['id'] => ['product_count' => $product['count']]
                ];
            }
            $order->products()->sync($products);
        }

        if($request->discount_id){
            $discount = DiscountCode::where('id', $request->discount_id)->firstOrFail();
            $input->discount_id = $discount->id;
        }

        $order->update($input);

        return response()->json($order, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(Request $request, string $id): JsonResponse
    {
        $order = Order::where('id', $id)->firstOrFail();

        $order->delete();

        return response()->json([],Response::HTTP_OK);
    }

    /**
     * @param OrderCreateRequest $request
     *
     * @return JsonResponse
     */
    public function create(OrderCreateRequest $request): JsonResponse
    {
        if($request->discount_id){
            DiscountCode::where('id', $request->discount_id)
                ->Where('count', '>', 0)->Where('active', true)->firstOrFail();
        }

        $user = auth('sanctum')->user();
        if(is_null($user)){
            $request->user_id = null;
        }else{
            if(!$user->is_admin){
                $request->user_id = $user->id;
            }
        }

        $order = Order::create([
            'status'            => 'created',
            'user_id'           => $request->user_id,
            'discount_id'       => $request->discount_id,
            'shipper_id'        => $request->shipper_id,
            'payment_method_id' => $request->payment_method_id,
            'first_name'        => $request->first_name,
            'last_name'         => $request->last_name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'address'           => $request->address,
            'zip'               => $request->zip,
            'city'              => $request->city,
            'shipping_cost'     => $request->shipping_cost,
            'discount'          => $request->discount,
            'total'             => $request->total,
            'state'             => $request->state,
            'country'           => $request->country
        ]);

        if($request->products){
            $products = [];
            foreach ($request->products as $product){
                Product::where('id',$product['id'])->firstOrFail();
                $products[$product['id']] = ['product_count' => $product['count']];
            }

            $order->products()->sync($products);
        }

        //TODO sending email

        return response()->json($order, Response::HTTP_CREATED);
    }
}
