<?php

namespace App\Http\Controllers;

use App\Http\Requests\Favorites\FavoriteCreateRequest;
use App\Models\Favorite;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FavoritesController extends Controller
{
    /**
     * @param FavoriteCreateRequest $request
     *
     * @return JsonResponse
     */
    public function create(FavoriteCreateRequest $request): JsonResponse
    {
        $favorite = Favorite::create([
            'product_id'   => $request->product_id,
            'user_id'      => $request->user_id,
        ]);

        return response()->json($favorite, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     *
     * @param string $productCode
     * @return JsonResponse
     */
    public function delete(Request $request, string $productCode): JsonResponse
    {
        $products = Product::where('slug', $productCode)->firstOrFail();

        $products->delete();

        return response()->json([],Response::HTTP_OK);
    }
}
