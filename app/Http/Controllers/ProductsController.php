<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductImageRequest;
use App\Http\Requests\ProductsRequest;
use App\Models\Image;
use App\Models\Product;
use App\Models\User;
use App\Services\WebPService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends Controller
{

    /**
     * Products list
     *
     * @param ProductsRequest $request
     * @return JsonResponse
     */
    public function list(ProductsRequest $request)
    {
        $user = auth('sanctum')->user();

        $with = ['brand'];

        if($request->image){
            if($request->image == 'all'){
                $with[] = 'images';
            }
            if($request->image == 'main'){
                $with[] = 'image';
            }
        }

        $products = Product::with($with)->orderBy('featured', 'desc')->limit($request->count);

        if($request->type){
            $products = $products->where('type', $request->type);
        }

        if(!is_null($user)){
            $products = $products->withCount(['favorites as is_favorite' => function ($query) use ($user) {
                return $query->where('user_id', $user->id);
            }]);
        }

        return response()->json($products->get());
    }

    /**
     * Show the product.
     * For a given user is_favorite flag additionally.
     *
     * @param Request $request
     * @param string $productCode
     * @return JsonResponse
     */
    public function single(Request $request, string $productCode)
    {
        $product = Product::where('slug', $productCode)->with(['images', 'brand', 'reviews', 'favorites']);

        $user = auth('sanctum')->user();
        if(!is_null($user)){
            $product = $product->withCount(['favorites as is_favorite' => function ($query) use ($user) {
                return $query->where('user_id', $user->id);
            }]);
        }

        return response()->json($product->firstOrFail());
    }

    /**
     *
     * @param ProductsRequest $request
     * @return JsonResponse
     */
    public function bestsellings(ProductsRequest $request)
    {
        $limit = $request->count;

        $orders = DB::table('orders__products')
            ->select('product_id', DB::raw('count(`product_count`) as counted'))
            ->groupBy('product_id')->orderBy('counted', 'desc')->limit($limit)
            ->get();

        $plucked = $orders->pluck('product_id')->all();

        $products = Product::whereIn('id', $plucked)->with(['brand','image'])->limit($limit)->get();

        if(count($plucked) < $limit){
            $top_viewed_products = Product::with(['brand','image'])
                ->orderBy('views', 'desc')->limit($limit - count($plucked))->get();

            $products = $products->merge($top_viewed_products);
        }

        return response()->json($products);
    }

    /**
     *
     * @param ProductsRequest $request
     * @return JsonResponse
     */
    public function recommends(ProductsRequest $request)
    {
        $user = auth('sanctum')->user();

        $products = Product::with(['brand','image'])->orderBy('views', 'desc')->limit($request->count);

        if(!is_null($user)){
            $orders = DB::table('orders')
                ->join('products', 'orders.product_id', '=', 'products.id')
                ->select(['products.tar', 'products.nicotine', 'products.type', 'products.discount', 'products.price'])
                ->where('orders.user_id', '=', $user)
                ->groupBy(['orders.product_id','orders.user_id'] )->limit(5)->with('brand')
                ->get();

            $products = $products->whereIn('type', $orders->pluck('type')->unique())
                ->whereBetween('tar', [$orders->min('tar'), $orders->max('tar')])
                ->whereBetween('nicotine', [$orders->min('nicotine'), $orders->max('nicotine')])
                ->whereBetween('price', [$orders->min('price'), $orders->max('price')]);
        }

        return response()->json($products->get());
    }

    /**
     *
     *
     * @param ProductImageRequest $request
     * @return JsonResponse
     */
    public function uploadImages(ProductImageRequest $request)
    {
        $image = $request->image;
        $name = WebPService::convert($request->image_alt,375,375, $image->path());

        $image = Image::create([
            'name' => $name,
            'alt'  => $request->image_alt,
            'main' => $request->main,
        ]);

        return response()->json($image, Response::HTTP_CREATED);
    }
//TODO deleteImage
    /*
     * Show comments for the product.
     *
     * @param  int  $product_id
     * @return \Illuminate\View\View
     */
    public function comments(string $productCode)
    {
        $comments = Product::where('slug', $productCode)->firstOrFail()->comments;

        return response()->json($comments);
    }

    /*
     * Show reviews for the product.
     *
     * @param  int  $product_id
     * @return \Illuminate\View\View
     */
    public function reviews(string $productCode)
    {
        $reviews = Product::where('slug', $productCode)->firstOrFail()->reviews;

        return response()->json($reviews);
    }

    /*
     * Show QAs for the product.
     *
     * @param  int  $product_id
     * @return \Illuminate\View\View
     */
    public function qa(string $productCode)
    {
        $reviews = Product::where('slug', $productCode)->firstOrFail()->qas;

        return response()->json($reviews);
    }

}
