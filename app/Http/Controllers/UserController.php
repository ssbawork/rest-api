<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function userProfile(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Update current authenticated User.
     *
     * @return JsonResponse
     */
    public function updateProfile(Request $request): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Change lang of the authenticated User.
     *
     * @return JsonResponse
     */
    public function changeLang(Request $request): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the favorites products of User.
     *
     * @return JsonResponse
     */
    public function favorites(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the view story of User.
     *
     * @return JsonResponse
     */
    public function story(): JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Get orders history of User.
     *
     * @return JsonResponse
     */
    public function orders(): JsonResponse
    {
        return response()->json(auth()->user());
    }
}
