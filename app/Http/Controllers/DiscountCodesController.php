<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiscountCodes\DiscountCodeCreateRequest;
use App\Http\Requests\DiscountCodes\DiscountCodeEditRequest;
use App\Http\Requests\DiscountCodes\DiscountCodesRequest;
use App\Models\DiscountCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DiscountCodesController extends Controller
{

    /**
     * @param DiscountCodesRequest $request
     *
     * @return JsonResponse
     */
    public function list(DiscountCodesRequest $request): JsonResponse
    {
        $discounts = new DiscountCode();

        if($request->code){
            $discounts = $discounts->where("code", $request->code);
        }

        if($request->count){
            $discounts = $discounts->where("count", $request->count);
        }

        if($request->untill){
            $discounts = $discounts->where("untill", $request->untill);
        }

        if($request->percent){
            $discounts = $discounts->where("percent", $request->percent);
        }

        if($request->value){
            $discounts = $discounts->where("value", $request->value);
        }

        if($request->active){
            $discounts = $discounts->where("active", $request->active);
        }


        return response()->json($discounts->get());
    }

    /**
     * @param Request $request
     *
     * @param string $code_or_id
     * @return JsonResponse
     */
    public function single(Request $request, string $code_or_id): JsonResponse
    {
        $discount = DiscountCode::where('id', $code_or_id)->orWhere('code', $code_or_id)->firstOrFail();

        return response()->json($discount);
    }

    /**
     * @param DiscountCodeCreateRequest $request
     *
     * @return JsonResponse
     */
    public function create(DiscountCodeCreateRequest $request): JsonResponse
    {
        $discount = DiscountCode::create([
            'code'    => $request->code,
            'count'   => $request->count,
            'untill'  => $request->untill,
            'percent' => $request->percent,
            'value'   => $request->value,
            'active'  => $request->active
        ]);

        return response()->json($discount, Response::HTTP_CREATED);
    }

    /**
     * @param DiscountCodeEditRequest $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function edit(DiscountCodeEditRequest $request, string $id): JsonResponse
    {
        $input = $request->validated();

        $discount = DiscountCode::where('id', $id)->firstOrFail();

        $discount->update($input);

        return response()->json($discount, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(Request $request, string $id): JsonResponse
    {
        $discount = DiscountCode::where('id', $id)->firstOrFail();

        $discount->delete();

        return response()->json([],Response::HTTP_OK);
    }



}
