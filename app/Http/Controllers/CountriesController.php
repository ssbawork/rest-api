<?php

namespace App\Http\Controllers;

use App\Http\Requests\Countries\CountriesRequest;
use App\Http\Requests\Countries\CountryCreateRequest;
use App\Http\Requests\Countries\CountryEditRequest;
use App\Models\Country;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountriesController extends Controller
{
    /**
     * Show countries list.
     *
     * @param CountriesRequest $request
     * @return JsonResponse
     */
    public function list(CountriesRequest $request): JsonResponse
    {
        $countries = new Country();

        if($request->name){
            $countries = $countries->whereLike('name', $request->name);
        }

        if($request->iso){
            $countries = $countries->where('iso', $request->iso);
        }

        if($request->delivery){
            $countries = $countries->where('delivery', boolval($request->delivery));
        }

        if($request->active){
            $countries = $countries->where('active', $request->active);
        }

        return response()->json($countries->get());
    }

    /**
     * Show single country.
     *
     * @param Request $request
     * @param string $country_iso
     * @return JsonResponse
     */
    public function single(Request $request, string $country_iso): JsonResponse
    {
        $country = Country::where('iso', $country_iso)->first();

        if(is_null($country)){
            return response()->json([],Response::HTTP_NOT_FOUND);
        }

        return response()->json($country);
    }

    /**
     * Create country.
     *
     * @param CountryCreateRequest $request
     * @return JsonResponse
     */
    public function create(CountryCreateRequest $request): JsonResponse
    {
        $country = Country::create([
            'name' => $request->name,
            'active' => $request->active ?? false,
            'iso' => $request->iso,
            'delivery' => $request->delivery ?? false
        ]);

        return response()->json($country, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param CountryEditRequest $request
     * @return JsonResponse
     */
    public function edit(CountryEditRequest $request, string $country_iso): JsonResponse
    {
        $input = $request->validated();

        $country = Country::where('iso', $country_iso)->firstOrFail();

        $country->update($input);

        return response()->json($country);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $country_iso
     * @return JsonResponse
     */
    public function delete(Request $request, string $country_iso): JsonResponse
    {
        $country = Country::where('iso', $country_iso)->firstOrFail();

        $country->delete();

        return response()->json([],Response::HTTP_OK);
    }
}
