<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentMethods\PaymentMethodCreateRequest;
use App\Http\Requests\PaymentMethods\PaymentMethodEditRequest;
use App\Http\Requests\PaymentMethods\PaymentMethodsRequest;
use App\Models\PaymentMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PaymentMethodsController extends Controller
{
    /**
     * Show payment methods list.
     *
     * @param PaymentMethodsRequest $request
     * @return JsonResponse
     */
    public function list(PaymentMethodsRequest $request): JsonResponse
    {
        $paymentMethods = new PaymentMethod();

        if($request->name){
            $paymentMethods = $paymentMethods->whereLike('name', $request->name);
        }

        if($request->slug){
            $paymentMethods = $paymentMethods->whereLike('slug', $request->slug);
        }

        if($request->min_cost){
            $paymentMethods = $paymentMethods->where("cost", '<=', $request->min_cost);
        }

        if($request->max_cost){
            $paymentMethods = $paymentMethods->where("cost", '>=', $request->max_cost);
        }

        if($request->cost_per_piece){
            $paymentMethods = $paymentMethods->where('cost_per_piece', boolval($request->cost_per_piece));
        }

        if($request->active){
            $paymentMethods = $paymentMethods->where('active', boolval($request->active));
        }

        return response()->json($paymentMethods->get());
    }

    /**
     * Show single payment method.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function single(Request $request, string $slug): JsonResponse
    {
        $paymentMethod = PaymentMethod::where('slug', $slug)->first();

        if(is_null($paymentMethod)){
            return response()->json([],Response::HTTP_NOT_FOUND);
        }

        return response()->json($paymentMethod);
    }

    /**
     * Create payment method.
     *
     * @param PaymentMethodCreateRequest $request
     * @return JsonResponse
     */
    public function create(PaymentMethodCreateRequest $request): JsonResponse
    {
        $paymentMethod = PaymentMethod::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'cost' => $request->cost,
            'cost_per_piece' => $request->cost_per_piece ?? false,
            'active' => $request->active ?? false,
        ]);

        return response()->json($paymentMethod, Response::HTTP_CREATED);
    }

    /**
     * Update
     *
     * @param PaymentMethodEditRequest $request
     * @param string $slug
     * @return JsonResponse
     */
    public function edit(PaymentMethodEditRequest $request, string $slug): JsonResponse
    {
        $input = $request->validated();

        $paymentMethod = PaymentMethod::where('slug', $slug)->firstOrFail();

        $paymentMethod->update($input);

        return response()->json($paymentMethod);
    }

    /**
     * Hard Delete.
     *
     * @param Request $request
     * @param string $slug
     * @return JsonResponse
     */
    public function delete(Request $request, string $slug): JsonResponse
    {
        $paymentMethod = PaymentMethod::where('slug', $slug)->firstOrFail();

        $paymentMethod->delete();

        return response()->json([],Response::HTTP_OK);
    }
}
