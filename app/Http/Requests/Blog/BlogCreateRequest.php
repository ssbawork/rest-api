<?php

namespace App\Http\Requests\Blog;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class BlogCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => ['nullable', 'string', 'exists:blog_categories,id'],
            'image'         => ['required', 'image', 'mimes:jpg,jpeg,png,webp'],
            'image_alt'     => ['required', 'string', 'max:255'],
            'name'          => ['required', 'string'],
            'slug'          => ['required', 'string', 'min:2', 'unique:blogs,slug'],
            'data'          => ['required', 'string', 'min:2'],
            'lang'          => ['required', 'string', 'min:2', 'max:2'],
            'country'       => ['nullable', 'string', 'exists:countries,iso'],
            'type'          => ['required', 'string'],
            'article_type'  => ['nullable', 'string'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
