<?php

namespace App\Http\Requests\ShippingMethods;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class ShippingMethodEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'     => ['nullable', 'string'],
            'slug'     => ['nullable', 'string', 'unique:payment_methods,slug'],
            'desc'     => ['nullable', 'string'],
            'time'     => ['nullable', 'numeric'],
            'cost'     => ['nullable', 'numeric'],
            'by_piece' => ['nullable', 'boolean'],
            'active'   => ['nullable', 'boolean'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
