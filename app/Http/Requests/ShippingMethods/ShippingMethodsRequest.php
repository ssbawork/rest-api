<?php

namespace App\Http\Requests\ShippingMethods;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class ShippingMethodsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'     => ['nullable', 'string'],
            'slug'     => ['nullable', 'string'],
            'desc'     => ['nullable', 'string'],
            'min_time' => ['nullable', 'numeric'],
            'max_time' => ['nullable', 'numeric'],
            'min_cost' => ['nullable', 'numeric'],
            'max_cost' => ['nullable', 'numeric'],
            'by_piece' => ['nullable', 'boolean'],
            'active'   => ['nullable', 'boolean'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
