<?php

namespace App\Http\Requests\Reviews;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class ReviewEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id'       => ['nullable', 'string'],
            'stars'         => ['nullable', 'numeric', 'min:1', 'max:5'],
            'feelings'      => ['nullable', 'string'],
            'pros'          => ['nullable', 'string'],
            'cons'          => ['nullable', 'string'],
            'review_score'  => ['nullable', 'numeric'],
            'status'        => ['nullable', 'in:created,published,spam'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
