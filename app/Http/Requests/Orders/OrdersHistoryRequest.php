<?php

namespace App\Http\Requests\Orders;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class OrdersHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status'            => ['nullable', 'string'],
            'code'              => ['nullable', 'string'],
            'anon'              => ['nullable', 'boolean'],
            'discount_id'       => ['nullable', 'string', 'exists:discount_codes,id'],
            'shipper_id'        => ['nullable', 'string', 'exists:shipping_methods,id'],
            'payment_method_id' => ['nullable', 'string', 'exists:payment_methods,id'],
            'total_min'         => ['nullable', 'numeric'],
            'total_max'         => ['nullable', 'string'],
            'email'             => ['nullable', 'string'],
            'phone'             => ['nullable', 'string'],
            'first_name'        => ['nullable', 'string'],
            'last_name'         => ['nullable', 'string'],
            'products'          => ['nullable', 'array'],
            'products.*'        => ['required_with:products', 'string', 'exists:products,id'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
