<?php

namespace App\Http\Requests\Orders;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'           => ['nullable', 'string', 'exists:users,id'],
            'discount_id'       => ['nullable', 'string', 'exists:discount_codes,id'],
            'shipper_id'        => ['required', 'string', 'exists:shipping_methods,id'],
            'payment_method_id' => ['required', 'string', 'exists:payment_methods,id'],
            'first_name'        => ['required', 'string'],
            'last_name'         => ['required', 'string'],
            'email'             => ['required', 'string'],
            'phone'             => ['nullable', 'string'],
            'address'           => ['required', 'string'],
            'zip'               => ['required', 'string'],
            'city'              => ['required', 'string'],
            'state'             => ['nullable', 'string'],
            'country'           => ['required', 'string'],
            'shipping_cost'     => ['required', 'numeric'],
            'discount'          => ['required', 'numeric'],
            'total'             => ['required', 'numeric'],
            'products'          => ['required', 'array'],
            'products.*.id'     => ['required_with:products', 'string', 'exists:products,id'],
            'products.*.count'  => ['required_with:products', 'numeric'],
        ];
    }

    /**
     * Fail validation response
     * @param Illuminate\Contracts\Validation\Validator
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), Response::HTTP_PRECONDITION_FAILED));
    }
}
