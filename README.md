https://matthewsetter.com/setup-step-debugging-php-xdebug3-docker/

docker-compose up -d --build app

1.cp .env.example .env 
2.docker-compose up
3.docker-compose exec app composer install
4.docker-compose exec app php artisan key:generate
5.docker-compose exec app php artisan jwt:secret
6.docker-compose exec app php artisan migrate:fresh --seed


# Plans

2. Revise seeders
3. Check code quality 
   1. Create DB schema
   2. Create missing components
4. implement Soil and Octane
5. Laravel Scout
6. UnitTests for Laravel Sanctum registration

====================
====================
====================

7. Collect all useragents
8. https://angular.io/guide/lazy-loading-ngmodules
   https://developer.mozilla.org/en-US/docs/Web/Performance/Lazy_loading
   https://netbasal.com/lazy-load-images-in-angular-with-two-lines-of-code-beb13cd5a1c4
   https://caniuse.com/loading-lazy-attr
   https://web.dev/i18n/ru/browser-level-image-lazy-loading/

## Articles Plans

1. API Authentication via Social Networks in Laravel 9 using Sanctum and Angular SSR | OAuth2 [Google](https://www.balbooa.com/gridbox-documentation/how-to-get-google-client-id-and-client-secret)  
2. Create static content for Angular SSR
   1. WebP for Angular <- Auto convert to WebP from 
   2. All fonts compile
3. Elastic search direct from Angular. db <-> ES   https://github.com/Jeroen-G/Explorer
```terminal
./ngrok http cigi-ssr-api.local:8093
```


ide-helper:eloquent    Add \Eloquent helper to \Eloquent\Model
ide-helper:generate    Generate a new IDE Helper file.
ide-helper:meta        Generate metadata for PhpStorm
ide-helper:models      Generate autocompletion for models
key

